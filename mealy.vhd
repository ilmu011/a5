library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
architecture mealy of payloadmarker is
    Signal di_ns          : std_logic_vector(1 downto 0);
    Signal di_cs          : std_logic_vector(1 downto 0) := (others => '0');
    Signal I_s            : std_logic_vector(1 downto 0);
    Signal S_ns           : std_logic_vector(1 downto 0);                    -- 4 Zustände: Min. 2 Bit
    Signal S_cs           : std_logic_vector(1 downto 0) := (others => '0'); -- ^    ^    ^    ^    ^
    Signal O_ns           : std_logic;
    Signal OClocked_cs    : std_logic := '0'; -- Einmal extra abtakten, da Mealy schneller ist; sonst kommt das Mark zu früh
    Signal O_cs           : std_logic := '0';
    Signal do_ns          : std_logic_vector(1 downto 0);
    Signal do_cs          : std_logic_vector(1 downto 0) := (others => '0');
    
    Signal CIt_1_cs  : std_logic_vector(1 downto 0) := (others => '0');
    Signal CIt_2_cs  : std_logic_vector(1 downto 0) := (others => '0');
    Signal CIt_3_cs  : std_logic_vector(1 downto 0) := (others => '0');
    Signal CIt_4_cs  : std_logic_vector(1 downto 0) := (others => '0');
    Signal CIt_5_cs  : std_logic_vector(1 downto 0) := (others => '0');
begin
    sn:
    process(I_s, S_cs) is
        variable input_v    : std_logic_vector(1 downto 0); --Immer 2 Bit
        variable state_v    : std_logic_vector(1 downto 0); --Abhängig von Typ
        variable newstate_v : std_logic_vector(1 downto 0); --Abhängig von Typ
        variable output_v   : std_logic;    --Immer 1 Bit
    begin
        --Schritt 1
            input_v := I_s;
            state_v := S_cs;
            newstate_v := S_cs;
        --Schritt 2
            --Übergang: <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                case state_v is
                    when "00" =>
                        if(input_v = "11") then
                            newstate_v := "01";
                        end if;
                        -- case input_v is
                            -- when "00" =>
                                -- newstate_v := "00";
                            -- when "01" =>
                                -- newstate_v := "00";
                            -- when "10" =>
                                -- newstate_v := "00";
                            -- when "11" =>
                                -- newstate_v := "01";
                            -- when others =>
                                -- fail
                        -- end case;
                    when "01" =>
                        if(input_v = "11") then
                            newstate_v := "10";
                        else
                            newstate_v := "00";
                        end if;
                        -- case input_v is
                            -- when "00" =>
                                -- newstate_v := "00";
                            -- when "01" =>
                                -- newstate_v := "00";
                            -- when "10" =>
                                -- newstate_v := "00";
                            -- when "11" =>
                                -- newstate_v := "10";
                            -- when others =>
                                -- fail
                                
                        -- end case;
                    when "10" =>
                        case input_v is
                            when "00" =>
                                newstate_v := "11";
                            when "01" =>
                                newstate_v := "00";
                            when "10" =>
                                newstate_v := "00";
                            when "11" =>
                                newstate_v := "10";
                            when others =>
                                --fail
                        end case;
                    when "11" =>
                        if(input_v = "11") then
                            newstate_v := "01";
                        else
                            newstate_v := "00";
                        end if;
                        -- case input_v is
                            -- when "00" =>
                                -- newstate_v := "00";
                            -- when "01" =>
                                -- newstate_v := "00";
                            -- when "10" =>
                                -- newstate_v := "00";
                            -- when "11" =>
                                -- newstate_v := "01";
                            -- when others =>
                                -- fail
                        -- end case;
                    when others =>
                        --fail
                end case;
            --Ausgang <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            if(state_v = "11" AND input_v = "00") then
                output_v := '1';
            else
                output_v := '0';
            end if;
        --Schritt 3
        S_ns <= newstate_v;
        O_ns <= output_v;
    end process sn;

    sequlo:
    process(clk) is
    begin
        if clk = '1' AND clk'event then
            if nres = '0' then
                di_cs <= (others => '0');
                S_cs  <= (others => '0');
                O_cs  <= '0';
                OClocked_cs  <= '0';
                do_cs <= (others => '0');
                
                --Schieberegister
                CIt_1_cs <= (others => '0');
                CIt_2_cs <= (others => '0');
                CIt_3_cs <= (others => '0');
                CIt_4_cs <= (others => '0');
                CIt_5_cs <= (others => '0');
            else
                di_cs          <= di_ns;
                S_cs           <= S_ns;    --ZUSTAND HIER
                O_cs           <= OClocked_cs;
                OClocked_cs    <= O_ns;
                do_cs          <= do_ns;
                
                --Schieberegister
                CIt_1_cs <= di_cs;
                Cit_2_cs <= CIt_1_cs;
                Cit_3_cs <= CIt_2_cs;
                Cit_4_cs <= CIt_3_cs;
                Cit_5_cs <= CIt_4_cs;
            end if;
        end if;
    end process sequlo;
    --Concurrent Signal Assignments
    di_ns <= di;
    mrk <= O_cs;
    do <= do_cs;
    I_s <= di_cs;
    do_ns <= di_cs;
    
    dip1 <= di_cs;
    dip2 <= CIt_1_cs;
    dip3 <= CIt_2_cs;
    dip4 <= CIt_3_cs;
    dip5 <= CIt_4_cs;
    dip6 <= CIt_5_cs;
    
    did <= "10";
end architecture mealy;