--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: payloadmarker_timesim.vhd
-- /___/   /\     Timestamp: Fri Jan 11 14:59:07 2019
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command  : -intstyle ise -rpw 100 -ar Structure -tm payloadmarker -w -dir netgen/fit -ofmt vhdl -sim payloadmarker.nga payloadmarker_timesim.vhd 
-- Device   : XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file   : payloadmarker.nga
-- Output file  : D:\DTP\a5\isemoore\iseWRK\netgen\fit\payloadmarker_timesim.vhd
-- # of Entities    : 1
-- Design Name  : payloadmarker.nga
-- Xilinx   : C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

-- entity payloadmarker is
  -- port (
    -- clk : in STD_LOGIC := 'X'; 
    -- nres : in STD_LOGIC := 'X'; 
    -- mrk : out STD_LOGIC; 
    -- di : in STD_LOGIC_VECTOR ( 1 downto 0 ); 
    -- did : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    -- dip1 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    -- dip2 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    -- dip3 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    -- dip4 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    -- dip5 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    -- dip6 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    -- do : out STD_LOGIC_VECTOR ( 1 downto 0 ) 
  -- );
-- end payloadmarker;

architecture moore_timesim of payloadmarker is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal di_0_II_UIM_3 : STD_LOGIC; 
  signal di_1_II_UIM_5 : STD_LOGIC; 
  signal nres_II_UIM_7 : STD_LOGIC; 
  signal did_0_MC_Q_9 : STD_LOGIC; 
  signal did_1_MC_Q_11 : STD_LOGIC; 
  signal dip1_0_MC_Q_13 : STD_LOGIC; 
  signal dip1_1_MC_Q_15 : STD_LOGIC; 
  signal dip2_0_MC_Q_17 : STD_LOGIC; 
  signal dip2_1_MC_Q_19 : STD_LOGIC; 
  signal dip3_0_MC_Q_21 : STD_LOGIC; 
  signal dip3_1_MC_Q_23 : STD_LOGIC; 
  signal dip4_0_MC_Q_25 : STD_LOGIC; 
  signal dip4_1_MC_Q_27 : STD_LOGIC; 
  signal dip5_0_MC_Q_29 : STD_LOGIC; 
  signal dip5_1_MC_Q_31 : STD_LOGIC; 
  signal dip6_0_MC_Q_33 : STD_LOGIC; 
  signal dip6_1_MC_Q_35 : STD_LOGIC; 
  signal do_0_MC_Q_37 : STD_LOGIC; 
  signal do_1_MC_Q_39 : STD_LOGIC; 
  signal mrk_MC_Q_41 : STD_LOGIC; 
  signal did_0_MC_Q_tsimrenamed_net_Q_42 : STD_LOGIC; 
  signal did_0_MC_D_43 : STD_LOGIC; 
  signal did_0_MC_D1_44 : STD_LOGIC; 
  signal did_0_MC_D2_45 : STD_LOGIC; 
  signal did_1_MC_Q_tsimrenamed_net_Q_46 : STD_LOGIC; 
  signal did_1_MC_D_47 : STD_LOGIC; 
  signal did_1_MC_D1_48 : STD_LOGIC; 
  signal did_1_MC_D2_49 : STD_LOGIC; 
  signal dip1_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip1_0_MC_UIM_51 : STD_LOGIC; 
  signal dip1_0_MC_D_52 : STD_LOGIC; 
  signal Gnd_53 : STD_LOGIC; 
  signal Vcc_54 : STD_LOGIC; 
  signal dip1_0_MC_D1_55 : STD_LOGIC; 
  signal dip1_0_MC_D2_56 : STD_LOGIC; 
  signal dip1_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip1_1_MC_UIM_58 : STD_LOGIC; 
  signal dip1_1_MC_D_59 : STD_LOGIC; 
  signal dip1_1_MC_D1_60 : STD_LOGIC; 
  signal dip1_1_MC_D2_61 : STD_LOGIC; 
  signal dip2_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip2_0_MC_D_63 : STD_LOGIC; 
  signal dip2_0_MC_D1_64 : STD_LOGIC; 
  signal dip2_0_MC_D2_65 : STD_LOGIC; 
  signal dip2_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip2_1_MC_D_67 : STD_LOGIC; 
  signal dip2_1_MC_D1_68 : STD_LOGIC; 
  signal dip2_1_MC_D2_69 : STD_LOGIC; 
  signal dip3_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip3_0_MC_UIM_71 : STD_LOGIC; 
  signal dip3_0_MC_D_72 : STD_LOGIC; 
  signal dip3_0_MC_D1_73 : STD_LOGIC; 
  signal dip3_0_MC_D2_74 : STD_LOGIC; 
  signal do_0_MC_UIM_75 : STD_LOGIC; 
  signal do_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal do_0_MC_D_77 : STD_LOGIC; 
  signal do_0_MC_D1_78 : STD_LOGIC; 
  signal do_0_MC_D2_79 : STD_LOGIC; 
  signal dip3_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip3_1_MC_UIM_81 : STD_LOGIC; 
  signal dip3_1_MC_D_82 : STD_LOGIC; 
  signal dip3_1_MC_D1_83 : STD_LOGIC; 
  signal dip3_1_MC_D2_84 : STD_LOGIC; 
  signal do_1_MC_UIM_85 : STD_LOGIC; 
  signal do_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal do_1_MC_D_87 : STD_LOGIC; 
  signal do_1_MC_D1_88 : STD_LOGIC; 
  signal do_1_MC_D2_89 : STD_LOGIC; 
  signal dip4_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip4_0_MC_UIM_91 : STD_LOGIC; 
  signal dip4_0_MC_D_92 : STD_LOGIC; 
  signal dip4_0_MC_D1_93 : STD_LOGIC; 
  signal dip4_0_MC_D2_94 : STD_LOGIC; 
  signal dip4_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip4_1_MC_UIM_96 : STD_LOGIC; 
  signal dip4_1_MC_D_97 : STD_LOGIC; 
  signal dip4_1_MC_D1_98 : STD_LOGIC; 
  signal dip4_1_MC_D2_99 : STD_LOGIC; 
  signal dip5_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip5_0_MC_UIM_101 : STD_LOGIC; 
  signal dip5_0_MC_D_102 : STD_LOGIC; 
  signal dip5_0_MC_D1_103 : STD_LOGIC; 
  signal dip5_0_MC_D2_104 : STD_LOGIC; 
  signal dip5_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip5_1_MC_UIM_106 : STD_LOGIC; 
  signal dip5_1_MC_D_107 : STD_LOGIC; 
  signal dip5_1_MC_D1_108 : STD_LOGIC; 
  signal dip5_1_MC_D2_109 : STD_LOGIC; 
  signal dip6_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip6_0_MC_D_111 : STD_LOGIC; 
  signal dip6_0_MC_D1_112 : STD_LOGIC; 
  signal dip6_0_MC_D2_113 : STD_LOGIC; 
  signal dip6_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip6_1_MC_D_115 : STD_LOGIC; 
  signal dip6_1_MC_D1_116 : STD_LOGIC; 
  signal dip6_1_MC_D2_117 : STD_LOGIC; 
  signal mrk_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal mrk_MC_D_119 : STD_LOGIC; 
  signal mrk_MC_D1_120 : STD_LOGIC; 
  signal mrk_MC_D2_121 : STD_LOGIC; 
  signal S_cs_FSM_FFd1_122 : STD_LOGIC; 
  signal S_cs_FSM_FFd1_MC_Q : STD_LOGIC; 
  signal S_cs_FSM_FFd1_MC_D_124 : STD_LOGIC; 
  signal S_cs_FSM_FFd1_MC_D1_125 : STD_LOGIC; 
  signal S_cs_FSM_FFd1_MC_D2_126 : STD_LOGIC; 
  signal S_cs_FSM_FFd3_127 : STD_LOGIC; 
  signal S_cs_FSM_FFd2_128 : STD_LOGIC; 
  signal S_cs_FSM_FFd3_MC_Q : STD_LOGIC; 
  signal S_cs_FSM_FFd3_MC_D_130 : STD_LOGIC; 
  signal S_cs_FSM_FFd3_MC_D1_131 : STD_LOGIC; 
  signal S_cs_FSM_FFd3_MC_D2_132 : STD_LOGIC; 
  signal S_cs_FSM_FFd2_MC_Q : STD_LOGIC; 
  signal S_cs_FSM_FFd2_MC_D_134 : STD_LOGIC; 
  signal S_cs_FSM_FFd2_MC_D1_135 : STD_LOGIC; 
  signal S_cs_FSM_FFd2_MC_D2_136 : STD_LOGIC; 
  signal S_cs_FSM_FFd2_MC_D2_PT_0_137 : STD_LOGIC; 
  signal S_cs_FSM_FFd2_MC_D2_PT_1_138 : STD_LOGIC; 
  signal S_cs_FSM_FFd2_MC_D2_PT_2_139 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN2 : STD_LOGIC; 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  di_0_II_UIM : X_BUF
    port map (
      I => di(0),
      O => di_0_II_UIM_3
    );
  di_1_II_UIM : X_BUF
    port map (
      I => di(1),
      O => di_1_II_UIM_5
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_7
    );
  did_0_Q : X_BUF
    port map (
      I => did_0_MC_Q_9,
      O => did(0)
    );
  did_1_Q : X_BUF
    port map (
      I => did_1_MC_Q_11,
      O => did(1)
    );
  dip1_0_Q : X_BUF
    port map (
      I => dip1_0_MC_Q_13,
      O => dip1(0)
    );
  dip1_1_Q : X_BUF
    port map (
      I => dip1_1_MC_Q_15,
      O => dip1(1)
    );
  dip2_0_Q : X_BUF
    port map (
      I => dip2_0_MC_Q_17,
      O => dip2(0)
    );
  dip2_1_Q : X_BUF
    port map (
      I => dip2_1_MC_Q_19,
      O => dip2(1)
    );
  dip3_0_Q : X_BUF
    port map (
      I => dip3_0_MC_Q_21,
      O => dip3(0)
    );
  dip3_1_Q : X_BUF
    port map (
      I => dip3_1_MC_Q_23,
      O => dip3(1)
    );
  dip4_0_Q : X_BUF
    port map (
      I => dip4_0_MC_Q_25,
      O => dip4(0)
    );
  dip4_1_Q : X_BUF
    port map (
      I => dip4_1_MC_Q_27,
      O => dip4(1)
    );
  dip5_0_Q : X_BUF
    port map (
      I => dip5_0_MC_Q_29,
      O => dip5(0)
    );
  dip5_1_Q : X_BUF
    port map (
      I => dip5_1_MC_Q_31,
      O => dip5(1)
    );
  dip6_0_Q : X_BUF
    port map (
      I => dip6_0_MC_Q_33,
      O => dip6(0)
    );
  dip6_1_Q : X_BUF
    port map (
      I => dip6_1_MC_Q_35,
      O => dip6(1)
    );
  do_0_Q : X_BUF
    port map (
      I => do_0_MC_Q_37,
      O => do(0)
    );
  do_1_Q : X_BUF
    port map (
      I => do_1_MC_Q_39,
      O => do(1)
    );
  mrk_42 : X_BUF
    port map (
      I => mrk_MC_Q_41,
      O => mrk
    );
  did_0_MC_Q : X_BUF
    port map (
      I => did_0_MC_Q_tsimrenamed_net_Q_42,
      O => did_0_MC_Q_9
    );
  did_0_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_0_MC_D_43,
      O => did_0_MC_Q_tsimrenamed_net_Q_42
    );
  did_0_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_did_0_MC_D_IN0,
      I1 => NlwBufferSignal_did_0_MC_D_IN1,
      O => did_0_MC_D_43
    );
  did_0_MC_D1 : X_ZERO
    port map (
      O => did_0_MC_D1_44
    );
  did_0_MC_D2 : X_ZERO
    port map (
      O => did_0_MC_D2_45
    );
  did_1_MC_Q : X_BUF
    port map (
      I => did_1_MC_Q_tsimrenamed_net_Q_46,
      O => did_1_MC_Q_11
    );
  did_1_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_1_MC_D_47,
      O => did_1_MC_Q_tsimrenamed_net_Q_46
    );
  did_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_1_MC_D_IN0,
      I1 => NlwBufferSignal_did_1_MC_D_IN1,
      O => did_1_MC_D_47
    );
  did_1_MC_D1 : X_ZERO
    port map (
      O => did_1_MC_D1_48
    );
  did_1_MC_D2 : X_ZERO
    port map (
      O => did_1_MC_D2_49
    );
  dip1_0_MC_Q : X_BUF
    port map (
      I => dip1_0_MC_Q_tsimrenamed_net_Q,
      O => dip1_0_MC_Q_13
    );
  dip1_0_MC_UIM : X_BUF
    port map (
      I => dip1_0_MC_Q_tsimrenamed_net_Q,
      O => dip1_0_MC_UIM_51
    );
  dip1_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip1_0_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip1_0_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip1_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_53
    );
  Vcc : X_ONE
    port map (
      O => Vcc_54
    );
  dip1_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip1_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip1_0_MC_D_IN1,
      O => dip1_0_MC_D_52
    );
  dip1_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip1_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip1_0_MC_D1_IN1,
      O => dip1_0_MC_D1_55
    );
  dip1_0_MC_D2 : X_ZERO
    port map (
      O => dip1_0_MC_D2_56
    );
  dip1_1_MC_Q : X_BUF
    port map (
      I => dip1_1_MC_Q_tsimrenamed_net_Q,
      O => dip1_1_MC_Q_15
    );
  dip1_1_MC_UIM : X_BUF
    port map (
      I => dip1_1_MC_Q_tsimrenamed_net_Q,
      O => dip1_1_MC_UIM_58
    );
  dip1_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip1_1_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip1_1_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip1_1_MC_Q_tsimrenamed_net_Q
    );
  dip1_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip1_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip1_1_MC_D_IN1,
      O => dip1_1_MC_D_59
    );
  dip1_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip1_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip1_1_MC_D1_IN1,
      O => dip1_1_MC_D1_60
    );
  dip1_1_MC_D2 : X_ZERO
    port map (
      O => dip1_1_MC_D2_61
    );
  dip2_0_MC_Q : X_BUF
    port map (
      I => dip2_0_MC_Q_tsimrenamed_net_Q,
      O => dip2_0_MC_Q_17
    );
  dip2_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip2_0_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip2_0_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip2_0_MC_Q_tsimrenamed_net_Q
    );
  dip2_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip2_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip2_0_MC_D_IN1,
      O => dip2_0_MC_D_63
    );
  dip2_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip2_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip2_0_MC_D1_IN1,
      O => dip2_0_MC_D1_64
    );
  dip2_0_MC_D2 : X_ZERO
    port map (
      O => dip2_0_MC_D2_65
    );
  dip2_1_MC_Q : X_BUF
    port map (
      I => dip2_1_MC_Q_tsimrenamed_net_Q,
      O => dip2_1_MC_Q_19
    );
  dip2_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip2_1_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip2_1_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip2_1_MC_Q_tsimrenamed_net_Q
    );
  dip2_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip2_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip2_1_MC_D_IN1,
      O => dip2_1_MC_D_67
    );
  dip2_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip2_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip2_1_MC_D1_IN1,
      O => dip2_1_MC_D1_68
    );
  dip2_1_MC_D2 : X_ZERO
    port map (
      O => dip2_1_MC_D2_69
    );
  dip3_0_MC_Q : X_BUF
    port map (
      I => dip3_0_MC_Q_tsimrenamed_net_Q,
      O => dip3_0_MC_Q_21
    );
  dip3_0_MC_UIM : X_BUF
    port map (
      I => dip3_0_MC_Q_tsimrenamed_net_Q,
      O => dip3_0_MC_UIM_71
    );
  dip3_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip3_0_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip3_0_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip3_0_MC_Q_tsimrenamed_net_Q
    );
  dip3_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip3_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip3_0_MC_D_IN1,
      O => dip3_0_MC_D_72
    );
  dip3_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip3_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip3_0_MC_D1_IN1,
      O => dip3_0_MC_D1_73
    );
  dip3_0_MC_D2 : X_ZERO
    port map (
      O => dip3_0_MC_D2_74
    );
  do_0_MC_Q : X_BUF
    port map (
      I => do_0_MC_Q_tsimrenamed_net_Q,
      O => do_0_MC_Q_37
    );
  do_0_MC_UIM : X_BUF
    port map (
      I => do_0_MC_Q_tsimrenamed_net_Q,
      O => do_0_MC_UIM_75
    );
  do_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_do_0_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_do_0_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => do_0_MC_Q_tsimrenamed_net_Q
    );
  do_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_do_0_MC_D_IN0,
      I1 => NlwBufferSignal_do_0_MC_D_IN1,
      O => do_0_MC_D_77
    );
  do_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_do_0_MC_D1_IN0,
      I1 => NlwBufferSignal_do_0_MC_D1_IN1,
      O => do_0_MC_D1_78
    );
  do_0_MC_D2 : X_ZERO
    port map (
      O => do_0_MC_D2_79
    );
  dip3_1_MC_Q : X_BUF
    port map (
      I => dip3_1_MC_Q_tsimrenamed_net_Q,
      O => dip3_1_MC_Q_23
    );
  dip3_1_MC_UIM : X_BUF
    port map (
      I => dip3_1_MC_Q_tsimrenamed_net_Q,
      O => dip3_1_MC_UIM_81
    );
  dip3_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip3_1_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip3_1_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip3_1_MC_Q_tsimrenamed_net_Q
    );
  dip3_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip3_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip3_1_MC_D_IN1,
      O => dip3_1_MC_D_82
    );
  dip3_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip3_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip3_1_MC_D1_IN1,
      O => dip3_1_MC_D1_83
    );
  dip3_1_MC_D2 : X_ZERO
    port map (
      O => dip3_1_MC_D2_84
    );
  do_1_MC_Q : X_BUF
    port map (
      I => do_1_MC_Q_tsimrenamed_net_Q,
      O => do_1_MC_Q_39
    );
  do_1_MC_UIM : X_BUF
    port map (
      I => do_1_MC_Q_tsimrenamed_net_Q,
      O => do_1_MC_UIM_85
    );
  do_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_do_1_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_do_1_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => do_1_MC_Q_tsimrenamed_net_Q
    );
  do_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_do_1_MC_D_IN0,
      I1 => NlwBufferSignal_do_1_MC_D_IN1,
      O => do_1_MC_D_87
    );
  do_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_do_1_MC_D1_IN0,
      I1 => NlwBufferSignal_do_1_MC_D1_IN1,
      O => do_1_MC_D1_88
    );
  do_1_MC_D2 : X_ZERO
    port map (
      O => do_1_MC_D2_89
    );
  dip4_0_MC_Q : X_BUF
    port map (
      I => dip4_0_MC_Q_tsimrenamed_net_Q,
      O => dip4_0_MC_Q_25
    );
  dip4_0_MC_UIM : X_BUF
    port map (
      I => dip4_0_MC_Q_tsimrenamed_net_Q,
      O => dip4_0_MC_UIM_91
    );
  dip4_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip4_0_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip4_0_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip4_0_MC_Q_tsimrenamed_net_Q
    );
  dip4_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip4_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip4_0_MC_D_IN1,
      O => dip4_0_MC_D_92
    );
  dip4_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip4_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip4_0_MC_D1_IN1,
      O => dip4_0_MC_D1_93
    );
  dip4_0_MC_D2 : X_ZERO
    port map (
      O => dip4_0_MC_D2_94
    );
  dip4_1_MC_Q : X_BUF
    port map (
      I => dip4_1_MC_Q_tsimrenamed_net_Q,
      O => dip4_1_MC_Q_27
    );
  dip4_1_MC_UIM : X_BUF
    port map (
      I => dip4_1_MC_Q_tsimrenamed_net_Q,
      O => dip4_1_MC_UIM_96
    );
  dip4_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip4_1_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip4_1_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip4_1_MC_Q_tsimrenamed_net_Q
    );
  dip4_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip4_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip4_1_MC_D_IN1,
      O => dip4_1_MC_D_97
    );
  dip4_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip4_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip4_1_MC_D1_IN1,
      O => dip4_1_MC_D1_98
    );
  dip4_1_MC_D2 : X_ZERO
    port map (
      O => dip4_1_MC_D2_99
    );
  dip5_0_MC_Q : X_BUF
    port map (
      I => dip5_0_MC_Q_tsimrenamed_net_Q,
      O => dip5_0_MC_Q_29
    );
  dip5_0_MC_UIM : X_BUF
    port map (
      I => dip5_0_MC_Q_tsimrenamed_net_Q,
      O => dip5_0_MC_UIM_101
    );
  dip5_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip5_0_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip5_0_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip5_0_MC_Q_tsimrenamed_net_Q
    );
  dip5_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip5_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip5_0_MC_D_IN1,
      O => dip5_0_MC_D_102
    );
  dip5_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip5_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip5_0_MC_D1_IN1,
      O => dip5_0_MC_D1_103
    );
  dip5_0_MC_D2 : X_ZERO
    port map (
      O => dip5_0_MC_D2_104
    );
  dip5_1_MC_Q : X_BUF
    port map (
      I => dip5_1_MC_Q_tsimrenamed_net_Q,
      O => dip5_1_MC_Q_31
    );
  dip5_1_MC_UIM : X_BUF
    port map (
      I => dip5_1_MC_Q_tsimrenamed_net_Q,
      O => dip5_1_MC_UIM_106
    );
  dip5_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip5_1_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip5_1_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip5_1_MC_Q_tsimrenamed_net_Q
    );
  dip5_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip5_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip5_1_MC_D_IN1,
      O => dip5_1_MC_D_107
    );
  dip5_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip5_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip5_1_MC_D1_IN1,
      O => dip5_1_MC_D1_108
    );
  dip5_1_MC_D2 : X_ZERO
    port map (
      O => dip5_1_MC_D2_109
    );
  dip6_0_MC_Q : X_BUF
    port map (
      I => dip6_0_MC_Q_tsimrenamed_net_Q,
      O => dip6_0_MC_Q_33
    );
  dip6_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip6_0_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip6_0_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip6_0_MC_Q_tsimrenamed_net_Q
    );
  dip6_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip6_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip6_0_MC_D_IN1,
      O => dip6_0_MC_D_111
    );
  dip6_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip6_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip6_0_MC_D1_IN1,
      O => dip6_0_MC_D1_112
    );
  dip6_0_MC_D2 : X_ZERO
    port map (
      O => dip6_0_MC_D2_113
    );
  dip6_1_MC_Q : X_BUF
    port map (
      I => dip6_1_MC_Q_tsimrenamed_net_Q,
      O => dip6_1_MC_Q_35
    );
  dip6_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip6_1_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_dip6_1_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => dip6_1_MC_Q_tsimrenamed_net_Q
    );
  dip6_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip6_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip6_1_MC_D_IN1,
      O => dip6_1_MC_D_115
    );
  dip6_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip6_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip6_1_MC_D1_IN1,
      O => dip6_1_MC_D1_116
    );
  dip6_1_MC_D2 : X_ZERO
    port map (
      O => dip6_1_MC_D2_117
    );
  mrk_MC_Q : X_BUF
    port map (
      I => mrk_MC_Q_tsimrenamed_net_Q,
      O => mrk_MC_Q_41
    );
  mrk_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_mrk_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_mrk_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => mrk_MC_Q_tsimrenamed_net_Q
    );
  mrk_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_mrk_MC_D_IN0,
      I1 => NlwBufferSignal_mrk_MC_D_IN1,
      O => mrk_MC_D_119
    );
  mrk_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_mrk_MC_D1_IN0,
      I1 => NlwBufferSignal_mrk_MC_D1_IN1,
      O => mrk_MC_D1_120
    );
  mrk_MC_D2 : X_ZERO
    port map (
      O => mrk_MC_D2_121
    );
  S_cs_FSM_FFd1 : X_BUF
    port map (
      I => S_cs_FSM_FFd1_MC_Q,
      O => S_cs_FSM_FFd1_122
    );
  S_cs_FSM_FFd1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd1_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_S_cs_FSM_FFd1_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => S_cs_FSM_FFd1_MC_Q
    );
  S_cs_FSM_FFd1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd1_MC_D_IN0,
      I1 => NlwBufferSignal_S_cs_FSM_FFd1_MC_D_IN1,
      O => S_cs_FSM_FFd1_MC_D_124
    );
  S_cs_FSM_FFd1_MC_D1 : X_AND6
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN0,
      I1 => NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN1,
      I2 => NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN2,
      I3 => NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN3,
      I4 => NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN4,
      I5 => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN5,
      O => S_cs_FSM_FFd1_MC_D1_125
    );
  S_cs_FSM_FFd1_MC_D2 : X_ZERO
    port map (
      O => S_cs_FSM_FFd1_MC_D2_126
    );
  S_cs_FSM_FFd3 : X_BUF
    port map (
      I => S_cs_FSM_FFd3_MC_Q,
      O => S_cs_FSM_FFd3_127
    );
  S_cs_FSM_FFd3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd3_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_S_cs_FSM_FFd3_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => S_cs_FSM_FFd3_MC_Q
    );
  S_cs_FSM_FFd3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd3_MC_D_IN0,
      I1 => NlwBufferSignal_S_cs_FSM_FFd3_MC_D_IN1,
      O => S_cs_FSM_FFd3_MC_D_130
    );
  S_cs_FSM_FFd3_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN0,
      I1 => NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN1,
      I2 => NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN2,
      O => S_cs_FSM_FFd3_MC_D1_131
    );
  S_cs_FSM_FFd3_MC_D2 : X_ZERO
    port map (
      O => S_cs_FSM_FFd3_MC_D2_132
    );
  S_cs_FSM_FFd2 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_MC_Q,
      O => S_cs_FSM_FFd2_128
    );
  S_cs_FSM_FFd2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd2_MC_REG_IN,
      CE => Vcc_54,
      CLK => NlwBufferSignal_S_cs_FSM_FFd2_MC_REG_CLK,
      SET => Gnd_53,
      RST => Gnd_53,
      O => S_cs_FSM_FFd2_MC_Q
    );
  S_cs_FSM_FFd2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D_IN0,
      I1 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D_IN1,
      O => S_cs_FSM_FFd2_MC_D_134
    );
  S_cs_FSM_FFd2_MC_D1 : X_ZERO
    port map (
      O => S_cs_FSM_FFd2_MC_D1_135
    );
  S_cs_FSM_FFd2_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN3,
      O => S_cs_FSM_FFd2_MC_D2_PT_0_137
    );
  S_cs_FSM_FFd2_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN4,
      O => S_cs_FSM_FFd2_MC_D2_PT_1_138
    );
  S_cs_FSM_FFd2_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN4,
      O => S_cs_FSM_FFd2_MC_D2_PT_2_139
    );
  S_cs_FSM_FFd2_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN0,
      I1 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN1,
      I2 => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN2,
      O => S_cs_FSM_FFd2_MC_D2_136
    );
  NlwBufferBlock_did_0_MC_D_IN0 : X_BUF
    port map (
      I => did_0_MC_D1_44,
      O => NlwBufferSignal_did_0_MC_D_IN0
    );
  NlwBufferBlock_did_0_MC_D_IN1 : X_BUF
    port map (
      I => did_0_MC_D2_45,
      O => NlwBufferSignal_did_0_MC_D_IN1
    );
  NlwBufferBlock_did_1_MC_D_IN0 : X_BUF
    port map (
      I => did_1_MC_D1_48,
      O => NlwBufferSignal_did_1_MC_D_IN0
    );
  NlwBufferBlock_did_1_MC_D_IN1 : X_BUF
    port map (
      I => did_1_MC_D2_49,
      O => NlwBufferSignal_did_1_MC_D_IN1
    );
  NlwBufferBlock_dip1_0_MC_REG_IN : X_BUF
    port map (
      I => dip1_0_MC_D_52,
      O => NlwBufferSignal_dip1_0_MC_REG_IN
    );
  NlwBufferBlock_dip1_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip1_0_MC_REG_CLK
    );
  NlwBufferBlock_dip1_0_MC_D_IN0 : X_BUF
    port map (
      I => dip1_0_MC_D1_55,
      O => NlwBufferSignal_dip1_0_MC_D_IN0
    );
  NlwBufferBlock_dip1_0_MC_D_IN1 : X_BUF
    port map (
      I => dip1_0_MC_D2_56,
      O => NlwBufferSignal_dip1_0_MC_D_IN1
    );
  NlwBufferBlock_dip1_0_MC_D1_IN0 : X_BUF
    port map (
      I => di_0_II_UIM_3,
      O => NlwBufferSignal_dip1_0_MC_D1_IN0
    );
  NlwBufferBlock_dip1_0_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip1_0_MC_D1_IN1
    );
  NlwBufferBlock_dip1_1_MC_REG_IN : X_BUF
    port map (
      I => dip1_1_MC_D_59,
      O => NlwBufferSignal_dip1_1_MC_REG_IN
    );
  NlwBufferBlock_dip1_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip1_1_MC_REG_CLK
    );
  NlwBufferBlock_dip1_1_MC_D_IN0 : X_BUF
    port map (
      I => dip1_1_MC_D1_60,
      O => NlwBufferSignal_dip1_1_MC_D_IN0
    );
  NlwBufferBlock_dip1_1_MC_D_IN1 : X_BUF
    port map (
      I => dip1_1_MC_D2_61,
      O => NlwBufferSignal_dip1_1_MC_D_IN1
    );
  NlwBufferBlock_dip1_1_MC_D1_IN0 : X_BUF
    port map (
      I => di_1_II_UIM_5,
      O => NlwBufferSignal_dip1_1_MC_D1_IN0
    );
  NlwBufferBlock_dip1_1_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip1_1_MC_D1_IN1
    );
  NlwBufferBlock_dip2_0_MC_REG_IN : X_BUF
    port map (
      I => dip2_0_MC_D_63,
      O => NlwBufferSignal_dip2_0_MC_REG_IN
    );
  NlwBufferBlock_dip2_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip2_0_MC_REG_CLK
    );
  NlwBufferBlock_dip2_0_MC_D_IN0 : X_BUF
    port map (
      I => dip2_0_MC_D1_64,
      O => NlwBufferSignal_dip2_0_MC_D_IN0
    );
  NlwBufferBlock_dip2_0_MC_D_IN1 : X_BUF
    port map (
      I => dip2_0_MC_D2_65,
      O => NlwBufferSignal_dip2_0_MC_D_IN1
    );
  NlwBufferBlock_dip2_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip2_0_MC_D1_IN0
    );
  NlwBufferBlock_dip2_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip1_0_MC_UIM_51,
      O => NlwBufferSignal_dip2_0_MC_D1_IN1
    );
  NlwBufferBlock_dip2_1_MC_REG_IN : X_BUF
    port map (
      I => dip2_1_MC_D_67,
      O => NlwBufferSignal_dip2_1_MC_REG_IN
    );
  NlwBufferBlock_dip2_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip2_1_MC_REG_CLK
    );
  NlwBufferBlock_dip2_1_MC_D_IN0 : X_BUF
    port map (
      I => dip2_1_MC_D1_68,
      O => NlwBufferSignal_dip2_1_MC_D_IN0
    );
  NlwBufferBlock_dip2_1_MC_D_IN1 : X_BUF
    port map (
      I => dip2_1_MC_D2_69,
      O => NlwBufferSignal_dip2_1_MC_D_IN1
    );
  NlwBufferBlock_dip2_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip2_1_MC_D1_IN0
    );
  NlwBufferBlock_dip2_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip1_1_MC_UIM_58,
      O => NlwBufferSignal_dip2_1_MC_D1_IN1
    );
  NlwBufferBlock_dip3_0_MC_REG_IN : X_BUF
    port map (
      I => dip3_0_MC_D_72,
      O => NlwBufferSignal_dip3_0_MC_REG_IN
    );
  NlwBufferBlock_dip3_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip3_0_MC_REG_CLK
    );
  NlwBufferBlock_dip3_0_MC_D_IN0 : X_BUF
    port map (
      I => dip3_0_MC_D1_73,
      O => NlwBufferSignal_dip3_0_MC_D_IN0
    );
  NlwBufferBlock_dip3_0_MC_D_IN1 : X_BUF
    port map (
      I => dip3_0_MC_D2_74,
      O => NlwBufferSignal_dip3_0_MC_D_IN1
    );
  NlwBufferBlock_dip3_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip3_0_MC_D1_IN0
    );
  NlwBufferBlock_dip3_0_MC_D1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_75,
      O => NlwBufferSignal_dip3_0_MC_D1_IN1
    );
  NlwBufferBlock_do_0_MC_REG_IN : X_BUF
    port map (
      I => do_0_MC_D_77,
      O => NlwBufferSignal_do_0_MC_REG_IN
    );
  NlwBufferBlock_do_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_do_0_MC_REG_CLK
    );
  NlwBufferBlock_do_0_MC_D_IN0 : X_BUF
    port map (
      I => do_0_MC_D1_78,
      O => NlwBufferSignal_do_0_MC_D_IN0
    );
  NlwBufferBlock_do_0_MC_D_IN1 : X_BUF
    port map (
      I => do_0_MC_D2_79,
      O => NlwBufferSignal_do_0_MC_D_IN1
    );
  NlwBufferBlock_do_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_do_0_MC_D1_IN0
    );
  NlwBufferBlock_do_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip1_0_MC_UIM_51,
      O => NlwBufferSignal_do_0_MC_D1_IN1
    );
  NlwBufferBlock_dip3_1_MC_REG_IN : X_BUF
    port map (
      I => dip3_1_MC_D_82,
      O => NlwBufferSignal_dip3_1_MC_REG_IN
    );
  NlwBufferBlock_dip3_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip3_1_MC_REG_CLK
    );
  NlwBufferBlock_dip3_1_MC_D_IN0 : X_BUF
    port map (
      I => dip3_1_MC_D1_83,
      O => NlwBufferSignal_dip3_1_MC_D_IN0
    );
  NlwBufferBlock_dip3_1_MC_D_IN1 : X_BUF
    port map (
      I => dip3_1_MC_D2_84,
      O => NlwBufferSignal_dip3_1_MC_D_IN1
    );
  NlwBufferBlock_dip3_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip3_1_MC_D1_IN0
    );
  NlwBufferBlock_dip3_1_MC_D1_IN1 : X_BUF
    port map (
      I => do_1_MC_UIM_85,
      O => NlwBufferSignal_dip3_1_MC_D1_IN1
    );
  NlwBufferBlock_do_1_MC_REG_IN : X_BUF
    port map (
      I => do_1_MC_D_87,
      O => NlwBufferSignal_do_1_MC_REG_IN
    );
  NlwBufferBlock_do_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_do_1_MC_REG_CLK
    );
  NlwBufferBlock_do_1_MC_D_IN0 : X_BUF
    port map (
      I => do_1_MC_D1_88,
      O => NlwBufferSignal_do_1_MC_D_IN0
    );
  NlwBufferBlock_do_1_MC_D_IN1 : X_BUF
    port map (
      I => do_1_MC_D2_89,
      O => NlwBufferSignal_do_1_MC_D_IN1
    );
  NlwBufferBlock_do_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_do_1_MC_D1_IN0
    );
  NlwBufferBlock_do_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip1_1_MC_UIM_58,
      O => NlwBufferSignal_do_1_MC_D1_IN1
    );
  NlwBufferBlock_dip4_0_MC_REG_IN : X_BUF
    port map (
      I => dip4_0_MC_D_92,
      O => NlwBufferSignal_dip4_0_MC_REG_IN
    );
  NlwBufferBlock_dip4_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip4_0_MC_REG_CLK
    );
  NlwBufferBlock_dip4_0_MC_D_IN0 : X_BUF
    port map (
      I => dip4_0_MC_D1_93,
      O => NlwBufferSignal_dip4_0_MC_D_IN0
    );
  NlwBufferBlock_dip4_0_MC_D_IN1 : X_BUF
    port map (
      I => dip4_0_MC_D2_94,
      O => NlwBufferSignal_dip4_0_MC_D_IN1
    );
  NlwBufferBlock_dip4_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip4_0_MC_D1_IN0
    );
  NlwBufferBlock_dip4_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip3_0_MC_UIM_71,
      O => NlwBufferSignal_dip4_0_MC_D1_IN1
    );
  NlwBufferBlock_dip4_1_MC_REG_IN : X_BUF
    port map (
      I => dip4_1_MC_D_97,
      O => NlwBufferSignal_dip4_1_MC_REG_IN
    );
  NlwBufferBlock_dip4_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip4_1_MC_REG_CLK
    );
  NlwBufferBlock_dip4_1_MC_D_IN0 : X_BUF
    port map (
      I => dip4_1_MC_D1_98,
      O => NlwBufferSignal_dip4_1_MC_D_IN0
    );
  NlwBufferBlock_dip4_1_MC_D_IN1 : X_BUF
    port map (
      I => dip4_1_MC_D2_99,
      O => NlwBufferSignal_dip4_1_MC_D_IN1
    );
  NlwBufferBlock_dip4_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip4_1_MC_D1_IN0
    );
  NlwBufferBlock_dip4_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip3_1_MC_UIM_81,
      O => NlwBufferSignal_dip4_1_MC_D1_IN1
    );
  NlwBufferBlock_dip5_0_MC_REG_IN : X_BUF
    port map (
      I => dip5_0_MC_D_102,
      O => NlwBufferSignal_dip5_0_MC_REG_IN
    );
  NlwBufferBlock_dip5_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip5_0_MC_REG_CLK
    );
  NlwBufferBlock_dip5_0_MC_D_IN0 : X_BUF
    port map (
      I => dip5_0_MC_D1_103,
      O => NlwBufferSignal_dip5_0_MC_D_IN0
    );
  NlwBufferBlock_dip5_0_MC_D_IN1 : X_BUF
    port map (
      I => dip5_0_MC_D2_104,
      O => NlwBufferSignal_dip5_0_MC_D_IN1
    );
  NlwBufferBlock_dip5_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip5_0_MC_D1_IN0
    );
  NlwBufferBlock_dip5_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip4_0_MC_UIM_91,
      O => NlwBufferSignal_dip5_0_MC_D1_IN1
    );
  NlwBufferBlock_dip5_1_MC_REG_IN : X_BUF
    port map (
      I => dip5_1_MC_D_107,
      O => NlwBufferSignal_dip5_1_MC_REG_IN
    );
  NlwBufferBlock_dip5_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip5_1_MC_REG_CLK
    );
  NlwBufferBlock_dip5_1_MC_D_IN0 : X_BUF
    port map (
      I => dip5_1_MC_D1_108,
      O => NlwBufferSignal_dip5_1_MC_D_IN0
    );
  NlwBufferBlock_dip5_1_MC_D_IN1 : X_BUF
    port map (
      I => dip5_1_MC_D2_109,
      O => NlwBufferSignal_dip5_1_MC_D_IN1
    );
  NlwBufferBlock_dip5_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip5_1_MC_D1_IN0
    );
  NlwBufferBlock_dip5_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip4_1_MC_UIM_96,
      O => NlwBufferSignal_dip5_1_MC_D1_IN1
    );
  NlwBufferBlock_dip6_0_MC_REG_IN : X_BUF
    port map (
      I => dip6_0_MC_D_111,
      O => NlwBufferSignal_dip6_0_MC_REG_IN
    );
  NlwBufferBlock_dip6_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip6_0_MC_REG_CLK
    );
  NlwBufferBlock_dip6_0_MC_D_IN0 : X_BUF
    port map (
      I => dip6_0_MC_D1_112,
      O => NlwBufferSignal_dip6_0_MC_D_IN0
    );
  NlwBufferBlock_dip6_0_MC_D_IN1 : X_BUF
    port map (
      I => dip6_0_MC_D2_113,
      O => NlwBufferSignal_dip6_0_MC_D_IN1
    );
  NlwBufferBlock_dip6_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip6_0_MC_D1_IN0
    );
  NlwBufferBlock_dip6_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip5_0_MC_UIM_101,
      O => NlwBufferSignal_dip6_0_MC_D1_IN1
    );
  NlwBufferBlock_dip6_1_MC_REG_IN : X_BUF
    port map (
      I => dip6_1_MC_D_115,
      O => NlwBufferSignal_dip6_1_MC_REG_IN
    );
  NlwBufferBlock_dip6_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip6_1_MC_REG_CLK
    );
  NlwBufferBlock_dip6_1_MC_D_IN0 : X_BUF
    port map (
      I => dip6_1_MC_D1_116,
      O => NlwBufferSignal_dip6_1_MC_D_IN0
    );
  NlwBufferBlock_dip6_1_MC_D_IN1 : X_BUF
    port map (
      I => dip6_1_MC_D2_117,
      O => NlwBufferSignal_dip6_1_MC_D_IN1
    );
  NlwBufferBlock_dip6_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip6_1_MC_D1_IN0
    );
  NlwBufferBlock_dip6_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip5_1_MC_UIM_106,
      O => NlwBufferSignal_dip6_1_MC_D1_IN1
    );
  NlwBufferBlock_mrk_MC_REG_IN : X_BUF
    port map (
      I => mrk_MC_D_119,
      O => NlwBufferSignal_mrk_MC_REG_IN
    );
  NlwBufferBlock_mrk_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_mrk_MC_REG_CLK
    );
  NlwBufferBlock_mrk_MC_D_IN0 : X_BUF
    port map (
      I => mrk_MC_D1_120,
      O => NlwBufferSignal_mrk_MC_D_IN0
    );
  NlwBufferBlock_mrk_MC_D_IN1 : X_BUF
    port map (
      I => mrk_MC_D2_121,
      O => NlwBufferSignal_mrk_MC_D_IN1
    );
  NlwBufferBlock_mrk_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_mrk_MC_D1_IN0
    );
  NlwBufferBlock_mrk_MC_D1_IN1 : X_BUF
    port map (
      I => S_cs_FSM_FFd1_122,
      O => NlwBufferSignal_mrk_MC_D1_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_REG_IN : X_BUF
    port map (
      I => S_cs_FSM_FFd1_MC_D_124,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_REG_IN
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_REG_CLK
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_D_IN0 : X_BUF
    port map (
      I => S_cs_FSM_FFd1_MC_D1_125,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_D_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_D_IN1 : X_BUF
    port map (
      I => S_cs_FSM_FFd1_MC_D2_126,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_D_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_D1_IN1 : X_BUF
    port map (
      I => dip1_0_MC_UIM_51,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_D1_IN2 : X_BUF
    port map (
      I => dip1_1_MC_UIM_58,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN2
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_D1_IN3 : X_BUF
    port map (
      I => S_cs_FSM_FFd1_122,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN3
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_D1_IN4 : X_BUF
    port map (
      I => S_cs_FSM_FFd3_127,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN4
    );
  NlwBufferBlock_S_cs_FSM_FFd1_MC_D1_IN5 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_128,
      O => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN5
    );
  NlwBufferBlock_S_cs_FSM_FFd3_MC_REG_IN : X_BUF
    port map (
      I => S_cs_FSM_FFd3_MC_D_130,
      O => NlwBufferSignal_S_cs_FSM_FFd3_MC_REG_IN
    );
  NlwBufferBlock_S_cs_FSM_FFd3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_S_cs_FSM_FFd3_MC_REG_CLK
    );
  NlwBufferBlock_S_cs_FSM_FFd3_MC_D_IN0 : X_BUF
    port map (
      I => S_cs_FSM_FFd3_MC_D1_131,
      O => NlwBufferSignal_S_cs_FSM_FFd3_MC_D_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd3_MC_D_IN1 : X_BUF
    port map (
      I => S_cs_FSM_FFd3_MC_D2_132,
      O => NlwBufferSignal_S_cs_FSM_FFd3_MC_D_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd3_MC_D1_IN1 : X_BUF
    port map (
      I => dip1_0_MC_UIM_51,
      O => NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd3_MC_D1_IN2 : X_BUF
    port map (
      I => dip1_1_MC_UIM_58,
      O => NlwBufferSignal_S_cs_FSM_FFd3_MC_D1_IN2
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_REG_IN : X_BUF
    port map (
      I => S_cs_FSM_FFd2_MC_D_134,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_REG_IN
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_REG_CLK
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D_IN0 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_MC_D1_135,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D_IN1 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_MC_D2_136,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => dip1_0_MC_UIM_51,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => dip1_1_MC_UIM_58,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => S_cs_FSM_FFd3_127,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => dip1_0_MC_UIM_51,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => dip1_1_MC_UIM_58,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => S_cs_FSM_FFd1_122,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_128,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => dip1_0_MC_UIM_51,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => dip1_1_MC_UIM_58,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => S_cs_FSM_FFd3_127,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_128,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_IN0 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_MC_D2_PT_0_137,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN0
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_IN1 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_MC_D2_PT_1_138,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN1
    );
  NlwBufferBlock_S_cs_FSM_FFd2_MC_D2_IN2 : X_BUF
    port map (
      I => S_cs_FSM_FFd2_MC_D2_PT_2_139,
      O => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_IN2
    );
  NlwInverterBlock_did_0_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_did_0_MC_D_IN0,
      O => NlwInverterSignal_did_0_MC_D_IN0
    );
  NlwInverterBlock_S_cs_FSM_FFd1_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN1,
      O => NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN1
    );
  NlwInverterBlock_S_cs_FSM_FFd1_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN2,
      O => NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN2
    );
  NlwInverterBlock_S_cs_FSM_FFd1_MC_D1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN3,
      O => NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN3
    );
  NlwInverterBlock_S_cs_FSM_FFd1_MC_D1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd1_MC_D1_IN4,
      O => NlwInverterSignal_S_cs_FSM_FFd1_MC_D1_IN4
    );
  NlwInverterBlock_S_cs_FSM_FFd2_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_S_cs_FSM_FFd2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_S_cs_FSM_FFd2_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_S_cs_FSM_FFd2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_S_cs_FSM_FFd2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_S_cs_FSM_FFd2_MC_D2_PT_2_IN2
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end moore_timesim;

