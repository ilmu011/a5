onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 30 {CLOCK & RESET}
add wave -noupdate -label CLOCK /payloadmarker_testbench/clk_s
add wave -noupdate -label RESET /payloadmarker_testbench/nres_s
add wave -noupdate -divider -height 30 INPUT
add wave -noupdate -label {DATA IN} -radix unsigned /payloadmarker_testbench/di_s
add wave -noupdate -divider -height 30 OUTPUT
add wave -noupdate -label {DATA OUT} -radix unsigned /payloadmarker_testbench/do_s
add wave -noupdate -label MARK /payloadmarker_testbench/mrk_s
add wave -noupdate -divider -height 150 <NULL>
add wave -noupdate -label AFTER1CC -radix unsigned -childformat {{/payloadmarker_testbench/dip1_s(1) -radix unsigned} {/payloadmarker_testbench/dip1_s(0) -radix unsigned}} -subitemconfig {/payloadmarker_testbench/dip1_s(1) {-radix unsigned} /payloadmarker_testbench/dip1_s(0) {-radix unsigned}} /payloadmarker_testbench/dip1_s
add wave -noupdate -label AFTER2CC -radix unsigned -childformat {{/payloadmarker_testbench/dip2_s(1) -radix unsigned} {/payloadmarker_testbench/dip2_s(0) -radix unsigned}} -subitemconfig {/payloadmarker_testbench/dip2_s(1) {-radix unsigned} /payloadmarker_testbench/dip2_s(0) {-radix unsigned}} /payloadmarker_testbench/dip2_s
add wave -noupdate -label AFTER3CC -radix unsigned -childformat {{/payloadmarker_testbench/dip3_s(1) -radix unsigned} {/payloadmarker_testbench/dip3_s(0) -radix unsigned}} -subitemconfig {/payloadmarker_testbench/dip3_s(1) {-radix unsigned} /payloadmarker_testbench/dip3_s(0) {-radix unsigned}} /payloadmarker_testbench/dip3_s
add wave -noupdate -label AFTER4CC -radix unsigned -childformat {{/payloadmarker_testbench/dip4_s(1) -radix unsigned} {/payloadmarker_testbench/dip4_s(0) -radix unsigned}} -subitemconfig {/payloadmarker_testbench/dip4_s(1) {-radix unsigned} /payloadmarker_testbench/dip4_s(0) {-radix unsigned}} /payloadmarker_testbench/dip4_s
add wave -noupdate -label AFTER5CC -radix unsigned -childformat {{/payloadmarker_testbench/dip5_s(1) -radix unsigned} {/payloadmarker_testbench/dip5_s(0) -radix unsigned}} -subitemconfig {/payloadmarker_testbench/dip5_s(1) {-radix unsigned} /payloadmarker_testbench/dip5_s(0) {-radix unsigned}} /payloadmarker_testbench/dip5_s
add wave -noupdate -label AFTER6CC -radix unsigned -childformat {{/payloadmarker_testbench/dip6_s(1) -radix unsigned} {/payloadmarker_testbench/dip6_s(0) -radix unsigned}} -subitemconfig {/payloadmarker_testbench/dip6_s(1) {-radix unsigned} /payloadmarker_testbench/dip6_s(0) {-radix unsigned}} /payloadmarker_testbench/dip6_s
add wave -noupdate -divider -height 150 <NULL>
add wave -noupdate -label DID -expand /payloadmarker_testbench/did_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1804 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {6272 ns}
