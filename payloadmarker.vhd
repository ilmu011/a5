library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
entity payloadmarker is                                         -- Device Under test 
    port ( 
        -- debugging only 
        did   : out std_logic_vector( 1 downto 0 );    -- Device Identifier             (optional) 
 
        dip1  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 1 clock cycle  (for debugging purpose only) 
        dip2  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 2 clock cycles (for debugging purpose only) 
        dip3  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 3 clock cycles (for debugging purpose only) 
        dip4  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 4 clock cycles (for debugging purpose only) 
        dip5  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 5 clock cycles (for debugging purpose only) 
        dip6  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 6 clock cycles (for debugging purpose only) 
        -- 
        do    : out std_logic_vector( 1 downto 0 );    -- Data Out 
        mrk   : out std_logic;                         -- MaRK of 1st data following pattern 
        -- 
        di    : in  std_logic_vector( 1 downto 0 );    -- Data In 
        -- 
        clk   : in  std_logic;                         -- CLocK 
        nres  : in  std_logic                          -- Not RESet ; low active reset 
    );--]port 
end entity payloadmarker;