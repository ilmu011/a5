library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
entity payloadmarker_testbench is
end entity payloadmarker_testbench;

architecture rtl of payloadmarker_testbench is  
    Signal di_s     : std_logic_vector(1 downto 0);
    Signal mrk_s    : std_logic;
    Signal do_s     : std_logic_vector(1 downto 0);
    
    Signal dip1_s   : std_logic_vector(1 downto 0);
    Signal dip2_s   : std_logic_vector(1 downto 0);
    Signal dip3_s   : std_logic_vector(1 downto 0);
    Signal dip4_s   : std_logic_vector(1 downto 0);
    Signal dip5_s   : std_logic_vector(1 downto 0);
    Signal dip6_s   : std_logic_vector(1 downto 0);
    
    Signal did_s    : std_logic_vector(1 downto 0);
    
    Signal clk_s    : std_logic;
    Signal nres_s   : std_logic;
    
    component sg is
        port (
        txData : out std_logic_vector( 1 downto 0 );   -- TxData send to DUT
        clk    : out std_logic;                        -- CLocK
        nres   : out std_logic                         -- Not RESet ; low active reset
    );--]port
    end component sg;
    
    for all : sg use entity work.sg(beh);
    
    component payloadmarker is
        port ( 
        -- debugging only 
        did   : out std_logic_vector( 1 downto 0 );    -- Device Identifier             (optional) 
 
        dip1  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 1 clock cycle  (for debugging purpose only) 
        dip2  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 2 clock cycles (for debugging purpose only) 
        dip3  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 3 clock cycles (for debugging purpose only) 
        dip4  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 4 clock cycles (for debugging purpose only) 
        dip5  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 5 clock cycles (for debugging purpose only) 
        dip6  : out std_logic_vector( 1 downto 0 );    -- "Data In" Plus 6 clock cycles (for debugging purpose only) 
        -- 
        do    : out std_logic_vector( 1 downto 0 );    -- Data Out 
        mrk   : out std_logic;                         -- MaRK of 1st data following pattern 
        -- 
        di    : in  std_logic_vector( 1 downto 0 );    -- Data In 
        -- 
        clk   : in  std_logic;                         -- CLocK 
        nres  : in  std_logic                          -- Not RESet ; low active reset 
    );--]port 
    end component payloadmarker;
    
    for all : payloadmarker use entity work.payloadmarker(moore_timesim);
    
begin
    sg_instance : sg
    port map(
        txData => di_s,
        clk => clk_s,
        nres => nres_s
    );
    
    payloadmarker_instance : payloadmarker
    port map(
        did => did_s,
        
        dip1 => dip1_s,
        dip2 => dip2_s,
        dip3 => dip3_s,
        dip4 => dip4_s,
        dip5 => dip5_s,
        dip6 => dip6_s,
        
        do => do_s,
        mrk => mrk_s,
        
        di => di_s,
        
        clk => clk_s,
        nres => nres_s
    );
    
end architecture rtl;