library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

architecture moore of payloadmarker is
    Signal di_ns    : std_logic_vector(1 downto 0);
    Signal di_cs    : std_logic_vector(1 downto 0) := (others => '0');
    Signal I_s      : std_logic_vector(1 downto 0);
    Signal S_ns     : std_logic_vector(2 downto 0);                    -- 5 Zustände: Min. 3 Bit
    Signal S_cs     : std_logic_vector(2 downto 0) := (others => '0'); -- ^    ^    ^    ^    ^
    Signal O_ns     : std_logic;
    Signal O_cs     : std_logic := '0';
    Signal do_ns    : std_logic_vector(1 downto 0);
    Signal do_cs    : std_logic_vector(1 downto 0) := (others => '0');
    
    Signal CIt_1_cs  : std_logic_vector(1 downto 0) := (others => '0');
    Signal CIt_2_cs  : std_logic_vector(1 downto 0) := (others => '0');
    Signal CIt_3_cs  : std_logic_vector(1 downto 0) := (others => '0');
    Signal CIt_4_cs  : std_logic_vector(1 downto 0) := (others => '0');
    Signal CIt_5_cs  : std_logic_vector(1 downto 0) := (others => '0');
begin
    usn:
    process(I_s, S_cs) is
        variable input_v    :   std_logic_vector(1 downto 0); --Immer 2 Bit
        variable state_v    :   std_logic_vector(2 downto 0); --Abhängig von Typ
        variable newstate_v :   std_logic_vector(2 downto 0); --Abhängig von Typ
    begin
        --Schritt 1
        input_v := I_s;
        state_v := S_cs;
        newstate_v := S_cs;
        --Schritt 2
        case state_v is
            when "000" =>
                if(input_v = "11") then
                    newstate_v := "001";
                end if;
                -- case input_v is
                    -- when "00" =>
                        -- newstate_v := "000";
                    -- when "01" =>
                        -- newstate_v := "000";
                    -- when "10" =>
                        -- newstate_v := "000";
                    -- when "11" =>
                        -- newstate_v := "001";
                    -- when others =>
                        -- fail
                -- end case;
            when "001" =>
                if(input_v = "11") then
                    newstate_v := "010";
                else
                    newstate_v := "000";
                end if;
                -- case input_v is
                    -- when "00" =>
                        -- newstate_v := "000";
                    -- when "01" =>
                        -- newstate_v := "000";
                    -- when "10" =>
                        -- newstate_v := "000";
                    -- when "11" =>
                        -- newstate_v := "010";
                    -- when others =>
                        -- fail
                -- end case;
            when "010" =>
                case input_v is
                    when "00" =>
                        newstate_v := "011";
                    when "01" =>
                        newstate_v := "000";
                    when "10" =>
                        newstate_v := "000";
                    when "11" =>
                        newstate_v := "010";
                    when others =>
                        --fail
                end case;
            when "011" =>
                case input_v is
                    when "00" =>
                        newstate_v := "100";
                    when "01" =>
                        newstate_v := "000";
                    when "10" =>
                        newstate_v := "000";
                    when "11" =>
                        newstate_v := "001";
                    when others =>
                        --fail
                end case;
            when "100" =>
                if(input_v = "11") then
                    newstate_v := "001";
                else
                    newstate_v := "000";
                end if;
                
                -- case input_v is
                    -- when "00" =>
                        -- newstate_v := "000";
                    -- when "01" =>
                        -- newstate_v := "000";
                    -- when "10" =>
                        -- newstate_v := "000";
                    -- when "11" =>
                        -- newstate_v := "001";
                    -- when others =>
                        -- fail
                -- end case;
            when others =>
                    --fail
        end case;
        --Schritt 3
        S_ns <= newstate_v;
    end process usn;
    
    asn:
    process(S_cs) is
        variable state_v    :   std_logic_vector(2 downto 0); --Abhängig von Typ
        variable output_v   :   std_logic; --Immer 1 Bit
    begin
        --Schritt1
        state_v := S_cs;
        output_v := '0';
        --Schritt2
        
        if( state_v = "100") then
            output_v := '1';
        end if;
        -- case state_v is
            -- when "000" =>
                -- output_v := '0';
            -- when "001" =>
                -- output_v := '0';
            -- when "010" =>
                -- output_v := '0';
            -- when "011" =>
                -- output_v := '0';
            -- when "100" =>
                -- output_v := '1';
            -- when others =>
                --fail
        -- end case;
        --Schritt3
        O_ns <= output_v;
        --O_ns <= ...
    end process asn;
    
    sequlo:
    process(clk) is
    begin
        if clk = '1' AND clk'event then
            if nres = '0' then
                di_cs <= (others => '0');
                S_cs  <= (others => '0');
                O_cs  <= '0';
                do_cs <= (others => '0');
                
                --Schieberegister
                CIt_1_cs <= (others => '0');
                CIt_2_cs <= (others => '0');
                CIt_3_cs <= (others => '0');
                CIt_4_cs <= (others => '0');
                CIt_5_cs <= (others => '0');
            else
                di_cs   <= di_ns;
                S_cs    <= S_ns;    --ZUSTAND HIER
                O_cs    <= O_ns;
                do_cs   <= do_ns;
                
                --Schieberegister
                CIt_1_cs <= di_cs;
                Cit_2_cs <= CIt_1_cs;
                Cit_3_cs <= CIt_2_cs;
                Cit_4_cs <= CIt_3_cs;
                Cit_5_cs <= CIt_4_cs;
            end if;
        end if;
    end process sequlo;
    --Concurrent Signal Assignments
    di_ns <= di;
    mrk <= O_cs;
    do <= do_cs;
    I_s <= di_cs;
    do_ns <= di_cs;
    
    dip1 <= di_cs;
    dip2 <= CIt_1_cs;
    dip3 <= CIt_2_cs;
    dip4 <= CIt_3_cs;
    dip5 <= CIt_4_cs;
    dip6 <= CIt_5_cs;
    
    did <= "01";
end architecture moore;